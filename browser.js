function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function getForecasts() {
	const results = {D: [], S: [], T: []};
	const models = ["D", "S", "T"];
	let modelIndex = 0;
	for(let model of models) {
		for(let order = 1; order <= 1; order++) {
			results[model].push([]);
			for(let testSet = 0; testSet <= 5; testSet++) {
				const orderInput = document.getElementById("max");
				orderInput.value = order;
				orderInput.dispatchEvent(new Event("input", {bubbles: true}));

				const testSetInput = document.getElementById("length");
				testSetInput.value = testSet;
				testSetInput.dispatchEvent(new Event("input", {bubbles: true}));
				
				document.querySelector(`#home > div > div.col-md-3 > div > div > div:nth-child(5) > span:nth-child(${modelIndex + 1})`).click();
				await timeout(1000);
				document.querySelector("#chart > div > div.amChartsLegend.amcharts-legend-div > svg > g > g > g:nth-child(2) > rect").onclick();
				await timeout(1000);
				results[model][order - 1].push([document.querySelector("#home > div > div.col-md-3 > p:nth-child(4)").innerText.substr(8), document.querySelector("#home > div > div.col-md-3 > p:nth-child(5)").innerText.substr(8)]);
			}
		}
		modelIndex++;
	}
	return results;
}
getForecasts().then(r => console.log(r));