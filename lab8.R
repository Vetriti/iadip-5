require(forecast)
require(zoo)
tsFile <- read.zoo(file = "ts34.csv", sep = ";", header=TRUE, tz="UTC", format="%d.%m.%Y", index.column = 1)
tsData <- ts(data = tsFile)
fit <- auto.arima(tsData)
fcast <- forecast(fit)
plot(fcast)
accuracy(fcast)

decomposed <- decompose(tsData)
plot(decomposed)


subseries <- ts(head(tsData,round(length(tsData)-length(tsData)*0.1),start=start(tsData),frequency=frequency(tsData)))
fcast <- forecast(auto.arima(subseries), h=round(length(tsData) * 0.1))
accuracy(f=fcast, x=tsData, SIMPLIFY=FALSE)
autoplot(fcast) + autolayer(tsTrain) + autolayer(tsTest)
